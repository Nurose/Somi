﻿using System;
using System.Collections.Generic;
using Nurose.Core;
using Nurose.ECS;

namespace Somi.Core
{
    public class ServiceContainer : Container<IService>
    {
        private readonly Dictionary<Type, IService> servicesByType = new();
        public SomiContext SomiContext;

        protected override void AddToAdds()
        {
            foreach (var service in toAdd)
                RegisterTypes(service);
            
            foreach (var service in toAdd)
                service.Setup();
            
            base.AddToAdds();
        }
        public override void Add(IService item)
        {
            item.SomiContext = SomiContext;
            base.Add(item);
        }
        private void RegisterTypes(IService service)
        {
            var queue = new Queue<Type>();
            Type type = service.GetType();
            queue.Enqueue(type);
            foreach (var interfaceType in type.GetInterfaces())
            {
                if (interfaceType == typeof(IService))
                    continue;

                queue.Enqueue(interfaceType);
            }

            foreach (var qtype in queue)
            {
                servicesByType.Add(qtype, service);
            }
        }

        public TService Get<TService>() where TService : class
        {
            if (!servicesByType.TryGetValue(typeof(TService), out IService service))
                throw new Exception($"Service of type: {typeof(TService).Name} not found.");

            return service as TService;
        }

    }
}