﻿using System;

namespace Somi.Core
{

    public interface IService
    {
        public SomiContext SomiContext { get; set; }
        public virtual void Setup() { }
        public virtual void PreDraw() { }
        public virtual void Draw() { }
    }
}