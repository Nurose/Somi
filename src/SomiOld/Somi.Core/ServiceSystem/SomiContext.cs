﻿namespace Somi.Core
{
    public class SomiContext
    {
        public ServiceContainer Services { get; private set; } = new();
        public T GetService<T>() where T : class
        {
            return Services.Get<T>();
        }
    }
}