﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.MSBuild;
using Microsoft.CodeAnalysis.Text;
using System;
using System.IO;
using System.Linq;

namespace Somi.Core
{
    public class RoslynContext
    {
        public Document SelectedDocument { get; set; }
        public SemanticModel SemanticModel { get; set; }
        public Workspace Workspace { get; set; }
        public CSharpCompilation CSharpCompilation { get; set; }

    }

    public class ContextService : IService
    {
        public SomiContext SomiContext { get; set; }

        public bool RoslynContextLoaded;
        public string SelectedFilePath { get; set; }
        public SourceText SourceText { get; set; }
        public RoslynContext RoslynContext = new();


        public event EventHandler<DocumentChangeArgs> OnActiveDocumentChange;


        public void LoadRoslynContext(Workspace workspace)
        {
            RoslynContext = new RoslynContext()
            {
                Workspace = workspace,
            };

            var doc = workspace.CurrentSolution.Projects.SelectMany(s => s.Documents).Where(d => d.FilePath.EndsWith("Program.cs")).OrderBy(o => o.Name).First();

            SwapToSemanticDocument(doc.FilePath);
        }

        private void SwapToSemanticDocument(string filePath)
        {
            SelectedFilePath = filePath;
            var document = RoslynContext.Workspace.CurrentSolution.Projects.SelectMany(p => p.Documents).Single(d => d.FilePath == filePath);

            SourceText = document.GetTextAsync().Result.WithChanges(new TextChange());
            RoslynContext.SelectedDocument = document;
            RoslynContext.SemanticModel = document.GetSemanticModelAsync().Result;
            RoslynContext.Workspace = RoslynContext.Workspace;
            RoslynContext.CSharpCompilation = RoslynContext.CSharpCompilation;
            RoslynContextLoaded = true;
            OnActiveDocumentChange?.Invoke(this, new DocumentChangeArgs());
            RoslynContext.Workspace.TryApplyChanges(RoslynContext.Workspace.CurrentSolution.WithDocumentText(RoslynContext.SelectedDocument.Id, SourceText));
        }

        private void SwapToPlainDocument(string filePath)
        {
            SelectedFilePath = filePath;
            SourceText = SourceText.From(File.ReadAllText(filePath));
            OnActiveDocumentChange?.Invoke(this, new DocumentChangeArgs());

        }

        public void SwapToDocument(string filePath)
        {
            if (filePath == SelectedFilePath)
                return;

            if (RoslynContextLoaded == true)
            {
                SwapToSemanticDocument(filePath);
            }
            else
            {
                SwapToPlainDocument(filePath);
            }
        }

        public void UpdateDocumentText(TextChange change)
        {
            SourceText = SourceText.WithChanges(change);
            RoslynContext.CSharpCompilation = (CSharpCompilation)RoslynContext.SemanticModel.Compilation;

            if (!RoslynContext.Workspace.TryApplyChanges(RoslynContext.Workspace.CurrentSolution.WithDocumentText(RoslynContext.SelectedDocument.Id, SourceText)))
                throw new Exception();

            RoslynContext.SelectedDocument = RoslynContext.Workspace.CurrentSolution.GetDocument(RoslynContext.SelectedDocument.Id);
            RoslynContext.SemanticModel = RoslynContext.SelectedDocument.GetSemanticModelAsync().Result;
        }
    }
}