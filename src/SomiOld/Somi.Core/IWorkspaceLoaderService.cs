﻿using Microsoft.CodeAnalysis;
using System.Threading.Tasks;

namespace Somi.Core
{
    public interface IWorkspaceLoaderService : IService
    {
        AdhocWorkspace Load(string solutionFilePath);
    }
}