﻿using Nurose.Core;
using Somi.Core;
using Somi.DefaultPlugins;  

namespace Somi.Desktop
{
    public class WindowBorderRenderer
    {
        public int Width = (int)NuroseMain.Monitor.ContentScale;
        public int TopBorderHeight = (int)(20* NuroseMain.Monitor.ContentScale);
        public Color Color = Utils.Lerp(Color.Magenta, Color.Black, .5f);

        public void Draw(RenderQueue renderQueue, string filePath)
        {
            var targetColor = Utils.Lerp(new Color(.5f,.0f,.5f), Color.Black, .0f);
            if (NuroseMain.Window.Input.MousePosition.IsBetweenRect(default, NuroseMain.Window.Size) && NuroseMain.Window.Input.MousePosition.Y < TopBorderHeight)
            {
                targetColor = Color.Red;
            }
            Color = Utils.Lerp(Color, targetColor, 20 * Time.DeltaTime);
            renderQueue.DrawRect(new Vector2(0, 0), new Vector2(Width, NuroseMain.Window.Size.Y), Color);
            renderQueue.DrawRect(new Vector2(NuroseMain.Window.Size.X - Width, 0), new Vector2(Width, NuroseMain.Window.Size.Y), Color);
            renderQueue.DrawRect(new Vector2(0, 0), new Vector2(NuroseMain.Window.Width, TopBorderHeight), Color);
            renderQueue.DrawRect(new Vector2(0, NuroseMain.Window.Size.Y - Width), new Vector2(NuroseMain.Window.Width, Width), Color);
            renderQueue.DrawText(filePath, new Vector2(NuroseMain.Window.Width / 2, 0), (int)(21* NuroseMain.Monitor.ContentScale), Color.White.WithAlpha(.99f), Fonts.Default, true);

            if (NuroseMain.Window.Input.MousePosition.Y < TopBorderHeight)
            {
                NuroseMain.Window.CursorShape = Nurose.Core.Cursor.Hand;
                if (NuroseMain.Window.Input.IsButtonPressed(MouseButton.Left))
                {
                    NuroseMain.Stop();
                }
            }
        }
    }
}