using System.Collections.Generic;
using System.Linq;
using Nurose.Core;

namespace Somi.DefaultPlugins
{
    public class UpdateCounter
    {
        private List<float> updates = new();
        private float validTime = 1;

        public float UpdatesPerSecond => updates.Count / validTime;
        public float AverageUpdateTime =>  validTime / updates.Count;
        
        public void Call()
        {
            while(updates.Count > 0 && updates[0] < Time.SecondsSinceStart - validTime)
                updates.RemoveAt(0);
            
            updates.Add(Time.SecondsSinceStart);
        }
    }
}