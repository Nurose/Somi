﻿using System;
using System.Collections.Generic;
using System.IO;
using Nurose.Core;
using Nurose.MsdfText;

namespace Somi.DefaultPlugins
{
    /// <summary>
    /// Caches token meshes for fast rendering.  
    /// </summary>
    public class TokenMeshCacher
    {
        private Dictionary<TokenMeshInfo, TokenMesh> cachedTokenMeshes = new();
        public MsdfTextDrawer generator;
    
        public TokenMeshCacher()
        {
            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory + "/Resources/Fonts/cascadia.fnt");
            generator = new MsdfTextDrawer(Fonts.Default);
            generator.Color = Color.White;
            generator.ScaleStyle = ScaleStyle.ForceLineHeight;
            generator.InvertedY = false;
        }

        public TokenMesh GetTokenMesh(TokenMeshInfo info)
        {
            if (!cachedTokenMeshes.ContainsKey(info))
            {
                generator.Text = info.Text;
                generator.Color = Color.White;
                
                cachedTokenMeshes.Add(info, new TokenMesh
                {
                    //RenderWidth = generator.CalcTextWidth(info.LineHeight, info.Text),
                    Vertices = generator.GetVertices()
                });
            }
            return cachedTokenMeshes[info];
        }
    }
}