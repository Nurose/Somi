﻿using System.Linq;
using Nurose.Core;

namespace Somi.DefaultPlugins
{
    /// <summary>
    /// Example <see cref="ICodeStyler"/> implementation.
    /// </summary>  
    public class RainbowCodeStyler : ICodeStyler
    {
        public int LineHeight { get; set; }

        public LineRenderInfo[] Style(StylerArgs args)
        {
            var tokens = args.SourceText.ToString().SplitInTokens();
            var helper = new StylerHelper(args.SourceText, LineHeight);

            int i = 0;
            foreach (var token in tokens)
            {
                var linepos = args.SourceText.Lines.GetLinePosition(i);
                helper.AddTokenRenderInfo(new TokenRenderInfo
                {
                    Coords = new Vector2Int(linepos.Character, linepos.Line),
                    SpanStart = i,
                    SpanEnd = i + token.Length,
                    TokenMeshInfo = new TokenMeshInfo
                    {
                        Text = token.ReplaceLineEndings(""),
                        LineHeight = LineHeight,
                        IsBold = false
                    },
                    Color = Color.FromHSL((i * 2) / 3 % 255.0f, 1, .5f),
                });
                i += token.Length;
            }
            return helper.ToLineRenderInfoArray();
        }
    }
}