﻿using Microsoft.CodeAnalysis;

namespace Somi.DefaultPlugins
{
    public static class SymbolExtentions
    {
        public static bool IsPrimitive(this INamedTypeSymbol symbol)
        {
            return symbol.SpecialType == SpecialType.System_Int16
                || symbol.SpecialType == SpecialType.System_Int32
                || symbol.SpecialType == SpecialType.System_Int64
                || symbol.SpecialType == SpecialType.System_Single
                || symbol.SpecialType == SpecialType.System_Double
                || symbol.SpecialType == SpecialType.System_Decimal
                || symbol.SpecialType == SpecialType.System_Enum
                || symbol.SpecialType == SpecialType.System_IntPtr
                || symbol.SpecialType == SpecialType.System_Single
                || symbol.SpecialType == SpecialType.System_Byte
                || symbol.SpecialType == SpecialType.System_Char;
        }
    }
}