﻿using Nurose.Core;

namespace Somi.DefaultPlugins
{
    public class DefaultSemanticCodeStyle : SemanticCodeStyle
    {
        public DefaultSemanticCodeStyle()
        {
            Add("struct name", new(Color.FromHexString("7CB36D")));
            Add("structure name", new(Color.FromHexString("7CB36D")));
            Add("enum name", new(Color.FromHexString("7CB36D")));
            Add("class name", new(Color.FromHexString("4EC9B0")));
            Add("type parameter name", new(Color.FromHexString("4EC9B0")));
            Add("string", new(Color.FromHexString("CE9178")));
            Add("keyword", new(Color.FromHexString("569CD6")));
            Add("keyword - control", new(Color.FromHexString("569CD6")));
            Add("operator", new(Color.FromHexString("D4D4D4")));
            Add("method name", new(Color.FromHexString("DCDCAA")));
            Add("property name", new(Color.FromHexString("DCDCAA")));
            Add("identifier", new(Color.White));
            Add("parameter name", new(Color.FromHexString("9CDCFE")));
            Add("enum member name", new(Color.FromHexString("DCDCAA")));
            Add("namespace name", new(Color.FromHexString("DCDCAA")));
            Add("field name", new(Color.FromHexString("DCDCAA")));
            Add("local name", new(Color.FromHexString("9CDCFE")));
            Add("comment", new(Color.FromHexString("6A9955")));
            Add("punctuation", new(Color.FromHexString("D4D4D4")));
            Add("number", new(Color.FromHexString("B5CEA8")));
            Add("xml doc comment - name", new(Color.FromHexString("569CD6")));
            Add("xml doc comment - text", new(Color.FromHexString("6A9955")));
            Add("xml doc comment - delimiter", new(Color.FromHexString("808080")));
        }
    }
}