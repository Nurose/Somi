﻿using System.Linq;
using Nurose.Core;

namespace Somi.DefaultPlugins
{
    public class PlainCodeStyler : ICodeStyler
    {
        public int LineHeight { get; set; }

        public LineRenderInfo[] Style(StylerArgs args)
        {
            var tokens = args.SourceText.ToString().SplitInTokens();
            var helper = new StylerHelper(args.SourceText, LineHeight);

            int i = 0;
            foreach (var token in tokens)
            {
                var linepos = args.SourceText.Lines.GetLinePosition(i);
                string renderedText = token.ReplaceLineEndings("");

                if (renderedText.Length != 0 && renderedText.Any(c => !c.Equals(' ')))
                    helper.AddTokenRenderInfo(new TokenRenderInfo
                    {
                        Coords = new Vector2Int(linepos.Character, linepos.Line),
                        SpanStart = i,
                        SpanEnd = i + token.Length,
                        TokenMeshInfo = new TokenMeshInfo
                        {
                            Text = renderedText,
                            LineHeight = LineHeight,
                            IsBold = false
                        },
                        Color = Color.White,
                    });
                i += token.Length;
            }
            return helper.ToLineRenderInfoArray();
        }
    }
}