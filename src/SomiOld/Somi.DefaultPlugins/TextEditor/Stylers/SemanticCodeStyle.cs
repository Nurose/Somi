﻿using Nurose.Core;
using System.Collections.Generic;

namespace Somi.DefaultPlugins
{
    public class SemanticCodeStyle
    {
        private Dictionary<string, SemanticCodeStyleEntry> entries = new();

        public SemanticCodeStyleEntry Get(string entryName)
        {
            if (!entries.TryGetValue(entryName, out SemanticCodeStyleEntry entry))
            {
                Logger.LogWarning($"Style entry: '{entryName}' not found");
                return new SemanticCodeStyleEntry(Color.FromHexString("9CDCFE"));
            }

            return entry;
        }

        public void Add(string entryName, SemanticCodeStyleEntry entry)
        {
            entries.Add(entryName, entry);
        }
    }
}