﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis.Text;
using Nurose.Core;

namespace Somi.DefaultPlugins
{
    public class StylerHelper
    {
        private int[] charAmountPerline;
        private int lineHeight;
        private Dictionary<int, List<TokenRenderInfo>> tokensByLine = new();
        private SourceText text;

        public StylerHelper(SourceText lines, int lineHeight)
        {
            this.text = lines;
            charAmountPerline = lines.ToString().Split("\r\n", StringSplitOptions.None).Select(l => l.Length + 2).ToArray();
            this.lineHeight = lineHeight;
        }

        public void AddTokenRenderInfo(TokenRenderInfo renderInfo)
        {
            if (renderInfo.TokenMeshInfo.Text.Contains("\n"))
            {
                SplitAndReAdd(renderInfo);
                return;
            }

            var coords = GetCoords(renderInfo.SpanStart);

            if (!tokensByLine.ContainsKey(coords.Y))
                tokensByLine.Add(coords.Y, new List<TokenRenderInfo>());

            if (!tokensByLine.ContainsKey(coords.Y))
                return;

            var line = tokensByLine[coords.Y];
            line.Add(renderInfo);
        }

        private void SplitAndReAdd(TokenRenderInfo renderInfo)
        {
            var lines = renderInfo.TokenMeshInfo.Text.Split('\n');
            var currentSpanStart = renderInfo.SpanStart;
            foreach (var line in lines)
            {
                var newRenderInfo = renderInfo;
                newRenderInfo.SpanStart = currentSpanStart+1;
                newRenderInfo.SpanEnd = currentSpanStart + line.Length+1;
                newRenderInfo.Coords = GetCoords(newRenderInfo.SpanStart);
                newRenderInfo.TokenMeshInfo.Text = text.GetSubText(new TextSpan(newRenderInfo.SpanStart, newRenderInfo.SpanEnd - newRenderInfo.SpanStart)).ToString().Replace("\n","");
                AddTokenRenderInfo(newRenderInfo);
                currentSpanStart += line.Length+1;
            }
        }

        public Vector2Int GetCoords(int charPos)
        {
            var y = 0;
            for (; y < charAmountPerline.Length; y++)
            {
                if (charPos < charAmountPerline[y])
                    break;

                charPos -= charAmountPerline[y];
            }
            return new Vector2Int(charPos, y);
        }

        public LineRenderInfo[] ToLineRenderInfoArray()
        {
            LineRenderInfo[] lineRenderInfos = new LineRenderInfo[charAmountPerline.Length];
            for (int i = 0; i < lineRenderInfos.Length; i++)
            {
                lineRenderInfos[i] = new LineRenderInfo();
                if (tokensByLine.ContainsKey(i))
                    lineRenderInfos[i].TokenRenderInfos = tokensByLine[i].ToArray();
                else
                    lineRenderInfos[i].TokenRenderInfos = new TokenRenderInfo[0];
            }
            return lineRenderInfos;
        }
    }
}