﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Classification;
using Microsoft.CodeAnalysis.FindSymbols;
using Microsoft.CodeAnalysis.Text;
using Somi.Core;
using System.Collections.Generic;
using System.Linq;

namespace Somi.DefaultPlugins
{

    public class SemanticCodeStyler : ICodeStyler
    {
        public int LineHeight { get; set; }
        private DefaultSemanticCodeStyle style = new DefaultSemanticCodeStyle();

        public LineRenderInfo[] Style(StylerArgs args)
        {
            var helper = new StylerHelper(args.SourceText, LineHeight);
            var semanticModel = args.SomiContext.GetService<ContextService>().RoslynContext.SemanticModel;
            var workspace = args.SomiContext.GetService<ContextService>().RoslynContext.Workspace;
            var spans = Classifier.GetClassifiedSpans(semanticModel, TextSpan.FromBounds(0, args.SourceText.Length), workspace).ToList();
            RemoveAdditiveSpans(spans);
            var text = semanticModel.SyntaxTree.GetText();

            foreach (var classifiedSpan in spans)
            {
                var tokenRenderInfo = new TokenRenderInfo
                {
                    SpanStart = classifiedSpan.TextSpan.Start,
                    SpanEnd = classifiedSpan.TextSpan.End,
                    Coords = helper.GetCoords(classifiedSpan.TextSpan.Start),
                    Color = style.Get(classifiedSpan.ClassificationType).Color,
                    TokenMeshInfo = new TokenMeshInfo
                    {
                        Text = text.GetSubText(classifiedSpan.TextSpan).ToString(),
                        LineHeight = LineHeight
                    }
                };

                if (tokenRenderInfo.TokenMeshInfo.Text.Contains("MathF"))
                {
                    int c = 5;
                }
                //if (classifiedSpan.ClassificationType.Contains("name"))
                {
                    var symbol = SymbolFinder.FindSymbolAtPositionAsync(args.Document, classifiedSpan.TextSpan.Start).Result;

                    if (symbol != null && symbol.Kind == SymbolKind.NamedType)
                    {
                        var namedtype = (INamedTypeSymbol)symbol;
                        if (!namedtype.IsPrimitive())
                        {
                            string entryName = namedtype.TypeKind.ToString().ToLower() + " name";
                            tokenRenderInfo.Color = style.Get(entryName).Color;
                        }

                    }
                }

                helper.AddTokenRenderInfo(tokenRenderInfo);
            }
            return helper.ToLineRenderInfoArray();
        }

        private static void RemoveAdditiveSpans(List<ClassifiedSpan> spans)
        {
            for (var i = spans.Count - 1; i >= 0; i--)
            {
                var span = spans[i];
                if (ClassificationTypeNames.AdditiveTypeNames.Contains(span.ClassificationType))
                {
                    spans.RemoveAt(i);
                }
            }
        }
    }
}