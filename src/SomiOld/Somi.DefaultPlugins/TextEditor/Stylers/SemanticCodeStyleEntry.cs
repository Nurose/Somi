﻿using Nurose.Core;

namespace Somi.DefaultPlugins
{
    public struct SemanticCodeStyleEntry
    {
        public Color Color;
        public bool IsBold;
        public bool IsItalic;


        public SemanticCodeStyleEntry(Color color)
        {
            Color = color;
            IsBold = false;
            IsItalic = false;
        }
    }
}