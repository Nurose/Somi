﻿using System.Collections.Generic;
using System.IO;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Text;
using Nurose.Core;

namespace Somi.DefaultPlugins
{
    /// <summary>
    /// Example <see cref="ICodeStyler"/> implementation.
    /// </summary>
    public class SyntaxTreeCodeStyler : ICodeStyler
    {
        public int LineHeight { get; set; }
        
        public LineRenderInfo[] Style(StylerArgs args)
        {
            var lines = args.SourceText;
            var helper = new StylerHelper(args.SourceText, LineHeight);

            var tree = CSharpSyntaxTree.ParseText(args.SourceText);


            foreach (var node in tree.GetRoot().DescendantTokens())
            {
                //  if(node.ChildTokens().Any())
                //     continue;

                var syntaxKind = node.Kind();

                var coords = helper.GetCoords(node.SpanStart);

                var renderInfo = new TokenRenderInfo
                {
                    Color = Color.Grey(.7f),
                    SpanStart = node.Span.Start,
                    SpanEnd = node.Span.End,
                    Coords = helper.GetCoords(node.Span.Start),
                    TokenMeshInfo = new TokenMeshInfo
                    {
                        Text = args.SourceText.GetSubText(node.Span).ToString().Replace("\r", "").Replace("\n", "").Replace(" ", ""),
                        LineHeight = LineHeight
                    },
                };



                if (syntaxKind == SyntaxKind.IdentifierToken)
                    renderInfo.Color = Color.White;
                if (syntaxKind == SyntaxKind.NumericLiteralToken)
                    renderInfo.Color = Color.Green;
                if (syntaxKind == SyntaxKind.PrivateKeyword)
                    renderInfo.Color = Color.Magenta;
                if (syntaxKind == SyntaxKind.PublicKeyword)
                    renderInfo.Color = Color.Magenta;
                if (syntaxKind == SyntaxKind.UsingKeyword)
                    renderInfo.Color = Color.Orange;


                helper.AddTokenRenderInfo(renderInfo);
            }

            return helper.ToLineRenderInfoArray();
        }
 
    }
}