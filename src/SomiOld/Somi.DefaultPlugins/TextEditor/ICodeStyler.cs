﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;
using Somi.Core;

namespace Somi.DefaultPlugins
{
    public struct StylerArgs
    {
        public SomiContext SomiContext;
        public Document Document;
        public SourceText SourceText;
    }
    
    public interface ICodeStyler
    {
        public int LineHeight { get; set; }
        public LineRenderInfo[] Style(StylerArgs args);
    }
}