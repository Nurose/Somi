﻿using Nurose.Core;

namespace Somi.DefaultPlugins
{
    public struct DrawArrowTask : IRenderTask
    {
        private readonly VertexBuffer LineBuffer;
        private readonly Vector2 start;
        private readonly Vector2 end;
        private readonly Color color;
        private readonly float headLength;
        private readonly float headAngle;

        public DrawArrowTask(VertexBuffer lineBuffer, Vector2 start, Vector2 end, Color color, float headLength, float headAngle)
        {
            LineBuffer = lineBuffer;
            this.start = start;
            this.end = end;
            this.color = color;
            this.headLength = headLength;
            this.headAngle = headAngle;
        }

        public void Execute(IRenderTarget rt)
        {
            var dir = (end - start).Normalized;
            var dir1 = Utils.DegreeToVector(Utils.VectorToDegree(dir) + headAngle) * headLength;
            var dir2 = Utils.DegreeToVector(Utils.VectorToDegree(dir) - headAngle) * headLength;

            rt.ModelMatrix = Matrix3x2.Identity;
            LineBuffer.SetVertices(new[]
            {
                new(new Vector3(start, .01f), color),
                new Vertex(new Vector3(end, .01f), color)
            });


            rt.RenderVertexBuffer(LineBuffer, PrimitiveType.LineStrip);
            LineBuffer.SetVertices(new[]
            {
                new(new Vector3(end - dir1, .01f), color),
                new Vertex(new Vector3(end, .01f), color),
                new Vertex(new Vector3(end - dir2, .01f), color)
            });
            rt.RenderVertexBuffer(LineBuffer, PrimitiveType.Triangles);
        }
    }
}