﻿using Nurose.Core;
using Nurose.MsdfText;

namespace Somi.DefaultPlugins
{

    public struct DrawTextTask : IRenderTask
    {
        public MsdfTextDrawer textDrawer;
        private readonly MsdfFont font;
        private readonly string text;
        private readonly bool centered;
        private readonly Color color;
        private readonly float depth;

        public DrawTextTask(MsdfTextDrawer textDrawer, MsdfFont font, string text, bool centered, Color color, float depth)
        {
            this.textDrawer = textDrawer;
            this.font = font;
            this.text = text;
            this.centered = centered;
            this.color = color;
            this.depth = depth;
        }

        public void Execute(IRenderTarget renderTarget)
        {
            textDrawer.MsdfFont = font;
            textDrawer.Text = text;
            textDrawer.Centered = centered;
            textDrawer.Color = color;
            textDrawer.InvertedY = false;
            textDrawer.ScaleStyle = ScaleStyle.ForceLineHeight;
            textDrawer.Depth = depth;
            textDrawer.Text = text;
            textDrawer.Execute(renderTarget);
        }
    }
}