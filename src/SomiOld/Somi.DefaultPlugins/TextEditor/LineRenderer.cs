﻿using System;
using Microsoft.CodeAnalysis.Text;
using Nurose.Core;

namespace Somi.DefaultPlugins
{
    public class LineRenderer
    {
        private TokenMeshCacher tokenMeshCacher = new();
        public LineRenderInfo[] LineInfos;
        private int lineHeight;
        private int lineMargin = 0;
        public Vector2 Offset = new Vector2(0, 0);
        private VertexBuffer VertexBuffer = new();

        public LineRenderer(int lineHeight)
        {
            this.lineHeight = lineHeight;
        }

        public void RenderLines(RenderQueue renderQueue)
        {
            Vector2 position = Offset.ToVector2Int();

            var charWidth = (int)tokenMeshCacher.generator.CalcCharWidth(lineHeight, '_');
            // RenderGrid(renderQueue);

            for (var i = 0; i < LineInfos.Length; i++)
            {
                var fullOffsetY = Offset.Y + i * (lineHeight + lineMargin);

                if (fullOffsetY > NuroseMain.Window.Size.Y || fullOffsetY < -lineHeight)
                    continue;
                position.Y = fullOffsetY;

                var lineInfo = LineInfos[i];
                foreach (var tokenRenderInfo in lineInfo.TokenRenderInfos)
                {
                    var tokenMesh = tokenMeshCacher.GetTokenMesh(tokenRenderInfo.TokenMeshInfo);

                    renderQueue.SetTexture(tokenMeshCacher.generator.MsdfFont.Texture);
                    Vector2 pos = Offset + tokenRenderInfo.Coords * (charWidth, lineHeight);
                    renderQueue.SetModelMatrix(new TransformData(pos, Vector2.One * lineHeight).CalcModelMatrix());
                    renderQueue.SetColorTint(tokenRenderInfo.Color);
                    renderQueue.UpdateVertexBuffer(VertexBuffer, tokenMesh.Vertices);
                    renderQueue.DrawVertexBuffer(VertexBuffer, PrimitiveType.Triangles);
                    //                    renderQueue.Add(new Drawable(tokenMesh.Mesh, tokenMeshCacher.generator.BMFont.Texture,
                    //                      , tokenRenderInfo.Color));

                    //  renderQueue.DrawRectWireFrames(pos, new Vector2(2,lineHeight),Color.Yellow,2);
                }
                var tx = 8;
                renderQueue.SetModelMatrix(new TransformData(new Vector2(tx, fullOffsetY), Vector2.One * lineHeight).CalcModelMatrix());
                renderQueue.SetColorTint(Color.Grey(.3f));
                var repeated = GetRepeated('0', 3 - i.ToString().Length);
                renderQueue.UpdateVertexBuffer(VertexBuffer, tokenMeshCacher.GetTokenMesh(new TokenMeshInfo
                {
                    Text = repeated,
                    LineHeight = lineHeight
                }).Vertices);
                tx += (int)tokenMeshCacher.generator.CalcTextWidth(lineHeight, repeated);
                renderQueue.DrawVertexBuffer(VertexBuffer, PrimitiveType.Triangles);

                renderQueue.SetModelMatrix(new TransformData(new Vector2(tx, fullOffsetY), Vector2.One * lineHeight).CalcModelMatrix());
                renderQueue.SetColorTint(Color.Grey(.6f));
                renderQueue.UpdateVertexBuffer(VertexBuffer, tokenMeshCacher.GetTokenMesh(new TokenMeshInfo
                {
                    Text = i.ToString(),
                    LineHeight = lineHeight
                }).Vertices);
                renderQueue.DrawVertexBuffer(VertexBuffer, PrimitiveType.Triangles);
                /* renderQueue.Add(new Drawable(tokenMeshCacher.GetTokenMesh(new TokenMeshInfo
                     {
                         Text = i.ToString(),
                         LineHeight = lineHeight
                     }).Mesh, tokenMeshCacher.generator.BMFont.Texture,
                     new TransformData(new Vector2(4, fullOffsetY), Vector2.One * lineHeight).CalcModelMatrix(), Color.Greyscale(.3f)));
                */
                position.X = Offset.X;
                position.Y += lineHeight;
            }
        }

        private void RenderGrid(RenderQueue renderQueue)
        {
            int charWidth = (int)tokenMeshCacher.generator.CalcCharWidth(lineHeight, '_');
            var c = Color.Grey(.3f);
            for (int y = 0; y < 30; y++)
            {
                for (int x = y % 2 == 0 ? 0 : 1; x < 60; x++)
                {
                    renderQueue.DrawRect(Offset + new Vector2(x * charWidth, y * lineHeight), new Vector2(charWidth, lineHeight), c);
                    x++;
                }
            }
        }

        private string GetRepeated(char c, int amount)
        {
            var s = "";
            for (int i = 0; i < amount; i++)
                s += c;

            return s;
        }

        private int GetStartOfLineIndex(SourceText text, int cursorLoc)
        {
            for (; cursorLoc > 0; cursorLoc--)
            {
                if (text[cursorLoc] == '\n')
                    return cursorLoc;
            }
            return 0;
        }
        private int GetNextLineIndex(SourceText text, int cursorLoc)
        {
            for (int c = cursorLoc; c < text.Length - 1; c++)
            {
                if (text[c] == '\n')
                    return c;
            }
            return -1;
        }
        private int GetLineIndex(SourceText text, int lineNumber)
        {
            var ln = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '\n')
                    ln++;

                if (ln == lineNumber)
                    return i;
            }
            return -1;
        }


        public int GetNearestCursorCoordinatesOfRenderPos(SourceText text, Vector2Int renderPos)
        {
            renderPos -= new Vector2Int((int)Offset.X, (int)Offset.Y);
            var lineNumber = renderPos.Y / lineHeight;
            var renderX = 0;

            var start = GetLineIndex(text, lineNumber);
            var max = GetNextLineIndex(text, start + 1) - 1;
            if (start == -1 || max == -1)
                return text.Length - 1;
            for (int i = start; i < max; i++)
            {
                var charWidth = (int)tokenMeshCacher.generator.CalcCharWidth(lineHeight, text[i]);
                if (renderX + charWidth > renderPos.X)
                {
                    return Math.Max(0, i);
                }
                renderX += charWidth;
            }
            
            return Utils.MinMax(0, text.Length, max);
        }


        public Vector2Int GetRenderPositionOfCoordinates(SourceText text, int charCoords)
        {
            var lineCoords = text.Lines.GetLinePosition(charCoords);
            int charWidth = (int)tokenMeshCacher.generator.CalcCharWidth(lineHeight, '_');
            return new Vector2Int((int)Utils.Round(Offset.X + lineCoords.Character * charWidth), (int)Utils.Round(Offset.Y + lineCoords.Line * lineHeight));
        }
    }
}