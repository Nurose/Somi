using System;
using Nurose.MsdfText;

namespace Somi.DefaultPlugins
{
    public static class Fonts
    {
        public static MsdfFont Default = MsdfFontGenerator.Generate(new MsdfFontGenerateArgs
        {
            FontPath = $"{AppDomain.CurrentDomain.BaseDirectory}/consolas.ttf",
            CharsetPath = $"{AppDomain.CurrentDomain.BaseDirectory}/charset.txt"
        });
    }
}