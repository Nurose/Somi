﻿using Nurose.Core;

namespace Somi.DefaultPlugins
{
    public class Cursor
    {
        public Vector2 LastOffsetPos;
        public Vector2 SmoothRenderPos;
        public int Index;
        public int TargetX;
        public float TimeSinceLastMoved = 0;
        public float SmoothWidth;
    }
}