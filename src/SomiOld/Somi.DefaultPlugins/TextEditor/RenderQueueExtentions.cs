﻿using Nurose.Core;
using Nurose.MsdfText;

namespace Somi.DefaultPlugins
{
    public static class RenderQueueExtentions
    {
        public static string FontName = "pixel";
        public static UnitRectangleDrawTask rectTask = new(Color.White, 1, false);
        public static UnitRectangleDrawTask rectTaskForLine = new(Color.White, 1, false);
        private static readonly VertexBuffer LineBuffer = new();
        private static VertexBuffer Outline { get; set; }
        public static MsdfTextDrawer textDrawer { get; set; } = new();
        private static ResourceManager ResourceManager { get; set; } = new();

        public static void DrawRect(this RenderQueue drawer, Vector2Int position, Vector2 size, Color color, ITexture texture = null)
        {
            if (texture == null)
                drawer.SetTexture(Texture.White1x1);
            else
                drawer.SetTexture(texture);

            drawer.Add(new SetColorTintTask(Color.White));
            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position, size).CalcModelMatrix()));
        }
        

        public static void DrawRect(this RenderQueue drawer, TransformData transformData, Color color, ITexture texture = null)
        {
            if (texture == null)
                drawer.SetTexture(Texture.White1x1);
            else
                drawer.SetTexture(texture);

            drawer.Add(new SetColorTintTask(Color.White));
            drawer.Add(new DrawRectTask(rectTask, color, transformData.CalcModelMatrix()));
        }

        public static void DrawCenteredRect(this RenderQueue drawer, Vector2 position, Color color, float radius, ITexture texture)
        {
            var radVector = new Vector2(radius, radius) * 1;

            var start = position - radVector;
            var end = position + radVector;

            drawer.DrawRect(start, end - start, color, texture);
        }


        public static void DrawRect(this RenderQueue drawer, Vector2 position, Vector2 size, Color color, ITexture texture, float rotation)
        {
            if (texture == null)
                drawer.SetTexture(Texture.White1x1);
            else
                drawer.SetTexture(texture);

            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position, size, rotation, Vector2.One / 2).CalcModelMatrix()));
        }


        public static void DrawArrow(this RenderQueue drawer, Vector2 start, Vector2 end, Color color, float headLength = .5f, float headAngle = 30)
        {
            drawer.SetTexture(Texture.White1x1);
            drawer.Add(new DrawArrowTask(LineBuffer, start, end, color, headLength, headAngle));
        }

        public static void DrawSimpleArrow(this RenderQueue drawer, Vector2 pos, Vector2 dir, Color color, float headLength = .5f, float headAngle = 30, float thinkness = 1)
        {
            var dir1 = Utils.DegreeToVector(Utils.VectorToDegree(dir) + headAngle) * headLength;
            var dir2 = Utils.DegreeToVector(Utils.VectorToDegree(dir) - headAngle) * headLength;

            var mid = pos + dir * headLength / 2;
            drawer.DrawLine(mid, mid - dir1, color, thinkness);
            drawer.DrawLine(mid, mid - dir2, color, thinkness);
            //drawer.DrawPoint(start,Color.Green, 5);
            //drawer.DrawPoint(end,Color.Red, 5);
        }

        public static void DrawLine(this RenderQueue drawer, Vector2 start, Vector2 end, Color color, float thinkness = 1)
        {
            drawer.SetTexture(Texture.White1x1);
            var dir = (end - start).Normalized;
            var size = new Vector2(Utils.Distance(start, end), thinkness);
            var matrix = new TransformData(start - new Vector2(-dir.Y,dir.X)*thinkness/2, size, -Utils.VectorToDegree(dir)).CalcModelMatrix() ;
            drawer.Add(new DrawRectTask(rectTaskForLine, color, matrix));
        }
        

        public static void DrawPoint(this RenderQueue drawer, Vector2 pos, Color color, float scale)
        {
            var size = Vector2.One * scale;
            drawer.SetTexture(Texture.White1x1);
            drawer.DrawRect(pos - size / 2, size, color);
        }

        public static float GetTextWidth(this RenderQueue drawer, string Text, int lineheight, ResourceManager resources)
        {
            return textDrawer.CalcTextWidth(lineheight, Text);
        }

        public static void DrawRectWireFrames(this RenderQueue drawer, Vector2 position, Vector2 size, Color color, float widht = 1)
        {
            drawer.SetTexture(Texture.White1x1);
            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position, new Vector2(widht, size.Y-widht)).CalcModelMatrix()));
            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position + new Vector2(widht,0), new Vector2(size.X-widht*2, widht)).CalcModelMatrix()));
            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position + new Vector2(size.X - widht, 0), new Vector2(widht, size.Y-widht)).CalcModelMatrix()));
            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position + new Vector2(0, size.Y - widht), new Vector2(size.X, widht)).CalcModelMatrix()));
        }

        public static void DrawRectWireFramesWithoutBottem(this RenderQueue drawer, Vector2 position, Vector2 size, Color color, int widht = 1)
        {
            drawer.SetTexture(Texture.White1x1);
            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position, (widht, size.Y)).CalcModelMatrix()));
            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position, (size.X, widht)).CalcModelMatrix()));
            drawer.Add(new DrawRectTask(rectTask, color, new TransformData(position + (size.X - widht, 0), (widht, size.Y + widht)).CalcModelMatrix()));
        }

        public static void DrawText(this RenderQueue drawer, string text, Vector2 position, int lineheight, Color color, MsdfFont font, bool centered = false)
        {
            drawer.SetModelMatrix(new TransformData(position, new Vector2(lineheight, lineheight)).CalcModelMatrix());
            drawer.SetColorTint(Color.White);
            drawer.Add(new DrawTextTask(textDrawer, font, text, centered, color, 1));
        }
    }
}