﻿namespace Somi.DefaultPlugins
{
    public abstract class AutoCompleteSuggestion
    {
        public abstract string Category { get; }
        public abstract string Name { set; }
        public abstract string GetDisplayText();

        public abstract string GetAutoCompleteText();
    }
}