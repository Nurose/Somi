﻿using Nurose.Core;

namespace Somi.DefaultPlugins
{
    /// <summary>
    /// <see cref="TokenRenderInfo"/> specifies the render state of a specific token. 
    /// </summary>
    public struct TokenRenderInfo
    {
        public TokenMeshInfo TokenMeshInfo;
        public int SpanStart;
        public int SpanEnd;
        public Vector2Int Coords;
        public Color Color;
    }
}