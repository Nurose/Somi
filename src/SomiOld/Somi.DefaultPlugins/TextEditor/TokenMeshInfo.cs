﻿namespace Somi.DefaultPlugins
{
    /// <summary>
    /// Represents info needed to build or fetch cached token meshes. Used by <see cref="TokenMeshCacher"/>
    /// </summary>
    public struct TokenMeshInfo
    {
        public string Text;
        public bool IsBold;
        public int LineHeight;
    }
}