﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ImGuiNET;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Completion;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.FindSymbols;
using Microsoft.CodeAnalysis.Host.Mef;
using Microsoft.CodeAnalysis.Recommendations;
using Microsoft.CodeAnalysis.Text;
using Nurose.Core;
using Somi.Core;

namespace Somi.DefaultPlugins
{
    public class Editor
    {
        private SomiContext SomiContext;

        private SourceText text => SomiContext.GetService<ContextService>().SourceText;
        private string loadedFilePath;
        private List<Cursor> Cursors;
        private string CurrentSelectedFilePath { get; set; }
        private LineRenderer lineRenderer;
        private ICodeStyler codeStyler;
        private Vector2 scrollTarget;
        private Vector2 offset = new Vector2(42 * NuroseMain.Monitor.ContentScale, 0);
        public Vector2Int Position;
        public Vector2Int Size = NuroseMain.Window.Size;

        private float scrollLerpSpeed = 40;
        private int lineHeight;
        private float scrollAmount = 21 * 2;
        private float scale;
        private char[] traversableChars = new[]
        {
            ' ',
            '.',
            '+',
            '-',
            ':',
            '<',
            '>',
            '(',
            ')',
            '[',
            ']',
            '\r',
            '\n',
            ';',
            ':',
            ','
        };
        private float timeSinceCursorMove = 0;
        private Key lastPressedKey;
        private Vector2 FlyingCursorPos;
        private Vector2 FlyingCursorVel;

        private Vector2 FlyingCursorBoxStart;
        private Vector2 FlyingCursorBoxEnd;

        private UpdateCounter updateCounter = new();


        public Editor(int lineHeight, SomiContext somiContext)
        {
            scale = NuroseMain.Monitor.ContentScale;
            this.SomiContext = somiContext;
            this.lineHeight = lineHeight;
            lineRenderer = new LineRenderer(lineHeight);
            codeStyler = new SemanticCodeStyler()
            {
                LineHeight = lineHeight
            };

            Cursors = new List<Cursor>()
            {
                new Cursor()
            };
            LoadLines();

            somiContext.GetService<ContextService>().OnActiveDocumentChange += (o, e) =>
            {
                Restyle();
            };

        }

        public void Draw()
        {

            ContextService contextService = SomiContext.GetService<ContextService>();
            if (loadedFilePath != contextService.SelectedFilePath)
            {
                Cursors[0] = new Cursor();
                offset.Y = 0;
                scrollTarget.Y = 0;
                loadedFilePath = contextService.SelectedFilePath;
            }
            var oldIndex = Cursors[0].Index;
            if (IsHovering())
            {
                NuroseMain.Window.CursorShape = Nurose.Core.Cursor.IBeam;
            }
            if (NuroseMain.Window.Input.IsButtonPressed(MouseButton.Left))
            {
                Cursors[0].Index = lineRenderer.GetNearestCursorCoordinatesOfRenderPos(text, NuroseMain.Window.Input.MousePosition);
                Cursors[0].TimeSinceLastMoved = 0;
            }

            if (NuroseMain.Window.Input.IsKeyHeld(Key.RightAlt) && contextService.RoslynContextLoaded)
            {
                var sm = contextService.RoslynContext.SemanticModel;
                var symbol = SymbolFinder.FindSymbolAtPositionAsync(sm, Cursors[0].Index, contextService.RoslynContext.Workspace).Result;
                ImGui.SetNextWindowSize(new System.Numerics.Vector2(300, 100) * scale);
                Vector2Int vector2Int = lineRenderer.GetRenderPositionOfCoordinates(text, Cursors[0].Index);
                ImGui.SetNextWindowPos(new System.Numerics.Vector2(vector2Int.X, vector2Int.Y));
                ImGui.Begin("Symbol Info");
                if (symbol != null)
                {
                    ImGui.Text($"Symbol Kind: {symbol.Kind}");
                    if (symbol.Kind == SymbolKind.NamedType)
                        ImGui.Text($"NamedSymbol TypeKind: {((INamedTypeSymbol)symbol).TypeKind}");
                }
                ImGui.End();
            }
            updateCounter.Call();
            // lineRenderer.LineInfos = codeStyler.Style(text);
            timeSinceCursorMove += Time.DeltaTime;
            LoadLinesIfNeeded();

            string charsInputtedLastFrame = NuroseMain.Window.Input.CharsInputtedLastFrame;
            if (!string.IsNullOrEmpty(charsInputtedLastFrame))
            {
                TextChange change = new TextChange(new TextSpan(Cursors[0].Index, 0), charsInputtedLastFrame);
                Cursors[0].Index += charsInputtedLastFrame.Length;
                UpdateText(change);
            }
            if (NuroseMain.Window.Input.IsKeyPressed(Key.Tab))
            {
                TextChange change = new TextChange(new TextSpan(Cursors[0].Index, 0), "   ");
                UpdateText(change);
                Cursors[0].Index += 3;
            }

            if (NuroseMain.Window.Input.IsKeyPressed(Key.Enter))
            {
                int amount = FindIndentationOfLine(Cursors[0].Index);
                if (amount > 0)
                    amount += 3;
                string newLineWithIndentation = "\r\n" + Indentation(amount);
                TextChange change = new TextChange(new TextSpan(Cursors[0].Index, 0), newLineWithIndentation);
                UpdateText(change);
                Cursors[0].Index += newLineWithIndentation.Length;
            }

            if (NuroseMain.Window.Input.IsKeyPressed(Key.Backspace))
            {
                int start;
                int length;
                if (NuroseMain.Window.Input.IsKeyHeld(Key.LeftControl))
                {
                    start = GetStartOfToken(Cursors[0].Index);
                    length = Cursors[0].Index - start;

                    while (text[start] == ' ')
                    {
                        length++;
                        start--;
                    }

                    if (text[Cursors[0].Index - 1] == '\n' && text[Cursors[0].Index - 2] == '\r')
                        length++;
                }
                else
                {

                    length = 1;
                    if (text[Cursors[0].Index - 1] == '\n' && text[Cursors[0].Index - 2] == '\r')
                        length = 2;

                    start = Cursors[0].Index - length;
                }
                TextChange change = new TextChange(new TextSpan(start, length), "");
                Cursors[0].Index -= length;
                UpdateText(change);
            }

            if (oldIndex != Cursors[0].Index)
                Cursors[0].TargetX = lineRenderer.GetRenderPositionOfCoordinates(text, Cursors[0].Index).X;

            DrawBackground();

            if (text != null)
            {
                UpdateScrollOffset();
                DrawLines();
            }
            UpdateCursors();
            DrawCursors();

            //RenderSuggestions(contextService);

            SomiContext.GetService<CustomRenderingService>()
                .RenderQueue.DrawText($"FPS: {MathF.Round(updateCounter.UpdatesPerSecond, 0)}", new Vector2(3, NuroseMain.Window.Height - lineHeight), lineHeight, Color.White, Fonts.Default);
        }

        private string Indentation(int amount)
        {
            char c = ' ';
            var s = new StringBuilder();
            s.Append(c, amount);
            return s.ToString();
        }

        private int FindIndentationOfLine(int cursor)
        {
            while (cursor > 0)
            {
                if (text[cursor] == '{')
                {
                    return cursor - GetStartOfLineIndex(cursor);
                }
                cursor--;
            }
            return 0;
        }

        private void RenderSuggestions(ContextService contextService)
        {
            CustomRenderingService customRenderingService = SomiContext.GetService<CustomRenderingService>();

            if (contextService.RoslynContextLoaded)
            {
                var hoveringIndex = lineRenderer.GetNearestCursorCoordinatesOfRenderPos(text, NuroseMain.Window.Input.MousePosition);

                var completionList = CompletionService.GetService(contextService.RoslynContext.SelectedDocument).GetCompletionsAsync(contextService.RoslynContext.SelectedDocument, Cursors[0].Index).Result;
                if (completionList != null && completionList.Items.Length > 0)
                {

                    Vector2 pos = lineRenderer.GetRenderPositionOfCoordinates(text, Cursors[0].Index);

                    string wor = text.GetSubText(new TextSpan(GetStartOfToken(Cursors[0].Index), Cursors[0].Index - GetStartOfToken(Cursors[0].Index))).ToString();

                    Vector2Int vector2Int = lineRenderer.GetRenderPositionOfCoordinates(text, Cursors[0].Index);
                    ImGui.SetNextWindowPos(new System.Numerics.Vector2(vector2Int.X, vector2Int.Y + lineHeight));
                    ImGui.SetNextWindowSize(new System.Numerics.Vector2(400, 800));

                    ImGui.PushStyleVar(ImGuiStyleVar.WindowBorderSize, 0);
                    ImGui.PushStyleVar(ImGuiStyleVar.WindowRounding, 0);
                    ImGui.PushStyleVar(ImGuiStyleVar.WindowTitleAlign, 0);
                    if (completionList != null)
                    {
                        ImGui.Begin("rec", ImGuiWindowFlags.NoTitleBar);
                        foreach (var item in completionList.Items.OrderBy(o => o.DisplayText))
                        {
                            ImGui.Text(item.DisplayTextPrefix + item.DisplayText + item.DisplayTextSuffix);
                        }
                        ImGui.End();
                    }
                    ImGui.PopStyleVar();
                }
            }
        }

        private void UpdateText(TextChange change)
        {
            Cursors[0].TimeSinceLastMoved = 0;
            SomiContext.GetService<ContextService>().UpdateDocumentText(change);
            Restyle();
        }

        private bool IsHovering()
        {
            return NuroseMain.Window.Input.MousePosition.IsBetweenRect(Position, Position + Size);
        }


        private void DrawCursors()
        {
            var customRenderingService = SomiContext.Services.Get<CustomRenderingService>();
            if (NuroseMain.Window.Input.IsKeyHeld(Key.LeftAlt))
            {
                var cursor = lineRenderer.GetNearestCursorCoordinatesOfRenderPos(text, new Vector2Int((int)FlyingCursorPos.X, (int)FlyingCursorPos.Y));
                var renderPositionOfCoordinates = lineRenderer.GetRenderPositionOfCoordinates(text, GetStartOfToken(cursor));
                //Context.RenderQueue.DrawRect(renderPositionOfCoordinates, new Vector2(4 * scale, lineHeight), Color.Orange, Texture.White1x1);
                customRenderingService.RenderQueue.DrawCenteredRect(FlyingCursorPos, Color.Orange, scale * 6, Texture.White1x1);
                customRenderingService.RenderQueue.DrawCenteredRect(FlyingCursorPos, Color.Black, scale * 4, Texture.White1x1);

                var wordStart = lineRenderer.GetRenderPositionOfCoordinates(text, GetStartOfToken(cursor));
                var wordEnd = lineRenderer.GetRenderPositionOfCoordinates(text, GetEndOfToken(cursor));


                //var target = new Vector2((wordEnd.X + wordStart.X) / 2, MathF.Floor(FlyingCursorPos.Y / lineHeight) * lineHeight + lineHeight / 2f);
                var renderPos = lineRenderer.GetRenderPositionOfCoordinates(text, cursor);
                if (renderPos.X > wordStart.X && renderPos.X < wordEnd.X)
                {
                    FlyingCursorBoxStart = Utils.Lerp(FlyingCursorBoxStart, wordStart, 20 * Time.DeltaTime);
                    FlyingCursorBoxEnd = Utils.Lerp(FlyingCursorBoxEnd, wordEnd, 20 * Time.DeltaTime);

                    var offset = new Vector2(scale, scale);
                    var size = new Vector2(FlyingCursorBoxEnd.X - FlyingCursorBoxStart.X + 2 * scale, lineHeight + 2 * scale);
                    customRenderingService.RenderQueue.DrawRectWireFrames(FlyingCursorBoxStart - offset, size, Color.Orange.WithAlpha(1), scale * 2);
                }
            }
            else
            {
                foreach (var cursor in Cursors)
                {
                    var realPos = lineRenderer.GetRenderPositionOfCoordinates(text, cursor.Index);
                    var pos = OvershootLerp(cursor.SmoothRenderPos, realPos, Time.DeltaTime * 2, 20);
                    cursor.SmoothRenderPos = pos;
                    if (cursor.TimeSinceLastMoved < .5 || cursor.TimeSinceLastMoved % 1f > .5f)
                    {
                        //    customRenderingService.RenderQueue.DrawLine(pos, realPos, Color.Orange, (int)(2 * scale));

                        //customRenderingService.RenderQueue.DrawRect(realPos - new Vector2Int((int)(1 * scale), 0), new Vector2Int((int)(2 * scale), lineHeight), Color.Orange);

                        float maxDistance = Utils.Difference(cursor.SmoothRenderPos.X, realPos.X);
                        float yDiff = Utils.Difference(cursor.SmoothRenderPos.Y, realPos.Y);
                        if (yDiff > maxDistance)
                            maxDistance = yDiff;

                        Cursors[0].SmoothWidth = OvershootLerp(Cursors[0].SmoothWidth, 3 * scale + maxDistance * 1, Time.DeltaTime * 1, 20);
                        float width = (int)Cursors[0].SmoothWidth;
                        int height = lineHeight;
                        customRenderingService.RenderQueue.DrawRect(pos - new Vector2(width / 2, 0), new Vector2(width, height), Color.Orange);
                    }
                }
            }
        }

        private float OvershootLerp(float a, float b, float c, float overshootStrength)
        {
            return Utils.Lerp(a, b + (b - a) * overshootStrength, Utils.MinMax(0, 1, c));
        }

        private Vector2 OvershootLerp(Vector2 a, Vector2 b, float c, float overshootStrength)
        {
            return Utils.Lerp(a, b + (b - a) * overshootStrength, Utils.MinMax(0, 1, c));
        }


        private bool IsKeyPressedRepeated(Key key)
        {
            if (NuroseMain.Window.Input.IsKeyPressed(key))
            {
                lastPressedKey = key;
                timeSinceCursorMove = -.3f;
                return true;
            }
            else if (NuroseMain.Window.Input.IsKeyHeld(key))
            {
                if (timeSinceCursorMove > .03f)
                {
                    timeSinceCursorMove = 0;
                    return true;
                }
            }
            return false;
        }

        private Vector2 GetMoveDir()
        {
            float xdir = 0;
            float ydir = 0;

            if (NuroseMain.Window.Input.IsKeyHeld(Key.Left)) xdir--;
            if (NuroseMain.Window.Input.IsKeyHeld(Key.Right)) xdir++;
            if (NuroseMain.Window.Input.IsKeyHeld(Key.Down)) ydir++;
            if (NuroseMain.Window.Input.IsKeyHeld(Key.Up)) ydir--;

            var targetDir = new Vector2(xdir, ydir);
            return targetDir.Normalized;
        }

        private void UpdateCursors()
        {
            if (NuroseMain.Window.Input.IsKeyPressed(Key.LeftAlt))
            {
                FlyingCursorPos = lineRenderer.GetRenderPositionOfCoordinates(text, Cursors[0].Index) + new Vector2Int(0, lineHeight / 2);
                FlyingCursorBoxStart = lineRenderer.GetRenderPositionOfCoordinates(text, GetStartOfToken(Cursors[0].Index));
                FlyingCursorBoxEnd = lineRenderer.GetRenderPositionOfCoordinates(text, GetEndOfToken(Cursors[0].Index));
            }
            if (NuroseMain.Window.Input.IsKeyReleased(Key.LeftAlt))
            {
                var cursor = lineRenderer.GetNearestCursorCoordinatesOfRenderPos(text, new Vector2Int((int)FlyingCursorPos.X, (int)FlyingCursorPos.Y));
                var renderPositionOfCoordinates = lineRenderer.GetRenderPositionOfCoordinates(text, cursor);
                Cursors[0].Index = cursor;
                Cursors[0].SmoothWidth = 100;
                Cursors[0].SmoothRenderPos = renderPositionOfCoordinates;
                Cursors[0].TargetX = cursor - GetStartOfLineIndex(cursor);
            }


            if (NuroseMain.Window.Input.IsKeyHeld(Key.LeftAlt))
            {
                FlyingCursorVel = OvershootLerp(FlyingCursorVel, GetMoveDir() * 8f, Time.DeltaTime * 10, 0f);
                // FlyingCursorVel += GetMoveDir() * 10f * Time.DeltaTime;
                FlyingCursorPos += FlyingCursorVel * Time.DeltaTime * 90;
                if (GetMoveDir().Y == 0 && GetMoveDir().X == 0)
                {
                    var cursor = lineRenderer.GetNearestCursorCoordinatesOfRenderPos(text, new Vector2Int((int)FlyingCursorPos.X, (int)FlyingCursorPos.Y));


                    var wordStart = lineRenderer.GetRenderPositionOfCoordinates(text, GetStartOfToken(cursor));
                    var wordEnd = lineRenderer.GetRenderPositionOfCoordinates(text, GetEndOfToken(cursor));

                    //var target = new Vector2((wordEnd.X + wordStart.X) / 2, MathF.Floor(FlyingCursorPos.Y / lineHeight) * lineHeight + lineHeight / 2f);
                    var renderPos = lineRenderer.GetRenderPositionOfCoordinates(text, cursor);
                    if (renderPos.X > wordStart.X && renderPos.X < wordEnd.X)
                    {
                        var target = new Vector2(FlyingCursorPos.X, MathF.Floor(FlyingCursorPos.Y / lineHeight) * lineHeight + lineHeight / 2f);
                        FlyingCursorPos = Utils.Lerp(FlyingCursorPos, target, Time.DeltaTime * 25);
                    }

                    FlyingCursorVel *= 1 - (Time.DeltaTime * 5);
                }
                var maxY = NuroseMain.Window.Size.Y - lineHeight * 2;
                var minY = lineHeight * 2;

                if (FlyingCursorPos.Y < minY)
                {
                    var of = new Vector2(0, FlyingCursorPos.Y - minY);
                    scrollTarget -= of;
                    FlyingCursorPos -= of;
                }

                if (FlyingCursorPos.Y > maxY)
                {
                    var of = new Vector2(0, FlyingCursorPos.Y - maxY);
                    scrollTarget -= of;
                    FlyingCursorPos -= of;
                }
            }
            else
            {
                foreach (var cursor in Cursors)
                {
                    cursor.TimeSinceLastMoved += Time.DeltaTime;
                    var newLoc = new Vector2Int();
                    var IsControl = NuroseMain.Window.Input.IsKeyHeld(Key.LeftControl) || NuroseMain.Window.Input.IsKeyHeld(Key.RightControl);

                    int prefIndex = cursor.Index;
                    if (IsKeyPressedRepeated(Key.Left) && cursor.Index > 0)
                    {
                        if (IsControl)
                        {
                            var off = GetStartOfToken(cursor.Index);
                            cursor.Index = off;
                        }
                        else
                        {
                            cursor.Index--;
                        }
                        cursor.Index = Utils.MinMax(0, text.Length - 1, cursor.Index);


                        while (text[cursor.Index] == '\n')
                            cursor.Index--;

                        var startOfLineIndex = GetStartOfLineIndex(cursor.Index);
                        var currentX = cursor.Index - startOfLineIndex;
                        cursor.TargetX = currentX;
                    }

                    if (IsKeyPressedRepeated(Key.Right) && cursor.Index < text.Length - 1)
                    {
                        if (IsControl)
                        {
                            cursor.Index = GetEndOfToken(cursor.Index);
                        }
                        else
                        {
                            cursor.Index++;
                        }
                        cursor.Index = Utils.MinMax(0, text.Length - 1, cursor.Index);

                        while (text[cursor.Index] == '\n' && cursor.Index < text.Length - 1)
                            cursor.Index++;

                        var startOfLineIndex = GetStartOfLineIndex(cursor.Index);
                        var currentX = cursor.Index - startOfLineIndex;
                        cursor.TargetX = currentX;
                    }


                    if (IsKeyPressedRepeated(Key.Up))
                    {
                        var startOfLineIndex = GetStartOfLineIndex(cursor.Index);
                        var currentX = cursor.Index - startOfLineIndex;
                        var prefLine = GetStartOfLineIndex(startOfLineIndex - 1);

                        if (RemainingCharsOnLine(prefLine) < currentX)
                            currentX = RemainingCharsOnLine(prefLine) - 1;

                        if (RemainingCharsOnLine(prefLine) >= cursor.TargetX)
                            currentX = cursor.TargetX;

                        if (prefLine != -1)
                            cursor.Index = prefLine + currentX;
                    }


                    if (IsKeyPressedRepeated(Key.Down))
                    {
                        var startOfLineIndex = GetStartOfLineIndex(cursor.Index);
                        var currentX = cursor.Index - startOfLineIndex;
                        var nextLine = GetNextLineIndex(cursor.Index);

                        if (RemainingCharsOnLine(nextLine) < currentX)
                            currentX = RemainingCharsOnLine(nextLine + 1) - 1;

                        if (RemainingCharsOnLine(nextLine) >= cursor.TargetX)
                            currentX = cursor.TargetX;

                        cursor.Index = nextLine + 1 + currentX;
                    }
                    if (prefIndex != cursor.Index)
                    {
                        cursor.TimeSinceLastMoved = 0;

                        var cursorRenderPos = lineRenderer.GetRenderPositionOfCoordinates(text, Cursors[0].Index);

                        if (cursorRenderPos.Y <= 0)
                            scrollTarget.Y -= cursorRenderPos.Y;
                        if (cursorRenderPos.Y >= NuroseMain.Window.Height - lineHeight)
                            scrollTarget.Y -= cursorRenderPos.Y - (NuroseMain.Window.Height - lineHeight);
                    }
                    cursor.Index = Utils.MinMax(0, text.Length - 1, cursor.Index);
                }


            }
        }

        private int GetStartOfToken(int index)
        {
            var off = index;
            var ch = text[off];
            while (off > 0)
            {
                off--;
                if (off > 1 && traversableChars.Contains(text[off - 1]))
                    break;

                if (traversableChars.Contains(text[off]))
                    break;
            }
            return off;
        }

        private int GetEndOfToken(int index)
        {
            var off = index;
            var ch = text[off];
            while (off < text.Length - 1)
            {
                off++;

                if (traversableChars.Contains(text[off - 1]) ||
                    traversableChars.Contains(text[off]))
                    break;
            }
            return off;
        }

        private int GetStartOfLineIndex(int cursorLoc)
        {
            cursorLoc--;
            for (; cursorLoc >= 0; cursorLoc--)
            {
                if (text[cursorLoc] == '\n')
                    return cursorLoc;

                if (cursorLoc == 0)
                    return 0;
            }
            return -1;
        }
        private int GetNextLineIndex(int cursorLoc)
        {
            for (int c = cursorLoc; c < text.Length - 1; c++)
            {
                if (text[c] == '\n')
                    return c;
            }
            return -1;
        }

        private int RemainingCharsOnLine(int cursorLoc)
        {
            var i = 0;
            for (int c = cursorLoc + 1; c < text.Length - 1; c++)
            {
                i++;
                if (text[c] == '\n')
                    return i;
            }
            return -1;
        }



        private void DrawBackground()
        {
            SomiContext.GetService<CustomRenderingService>().RenderQueue.DrawRect(Position, Size, Color.FromBytes(26, 24, 29));
        }

        private void LoadLinesIfNeeded()
        {
            if (CurrentSelectedFilePath != SomiContext.GetService<ContextService>().SelectedFilePath)
            {
                LoadLines();
            }
        }
        private void LoadLines()
        {
            CurrentSelectedFilePath = SomiContext.GetService<ContextService>().SelectedFilePath;
            //TODO SomiContext.GetService<ContextService>().SourceText = SourceText.From(File.ReadAllText(CurrentSelectedFilePath));
            Restyle();
        }

        private void Restyle()
        {
            ContextService contextService = SomiContext.GetService<ContextService>();
            ICodeStyler styler = codeStyler;
            if (!contextService.RoslynContextLoaded)
            {
                styler = new PlainCodeStyler() { LineHeight = lineHeight };
            }
            else
            {
                int c = 5;
            }

            lineRenderer.LineInfos = styler.Style(new StylerArgs
            {
                SomiContext = SomiContext,
                Document = contextService.RoslynContextLoaded ? contextService.RoslynContext.SelectedDocument : null,
                SourceText = text,
            });
        }

        private void DrawLines()
        {
            lineRenderer.Offset = Position + offset;
            lineRenderer.RenderLines(SomiContext.GetService<CustomRenderingService>().RenderQueue);
        }

        private Vector2Int GetRoundedOffset()
        {
            return new Vector2Int((int)MathF.Round(offset.X), (int)MathF.Round(offset.Y));
        }

        private void UpdateScrollOffset()
        {
            scrollTarget = new Vector2(offset.X, scrollTarget.Y + NuroseMain.Window.Input.MouseWheelDelta * scrollAmount);

            var clamped = new Vector2(offset.X, Utils.MinMax(-text.Length * lineHeight + NuroseMain.Window.Size.Y - Position.Y, 0, scrollTarget.Y));
            scrollTarget = Utils.Lerp(scrollTarget, clamped, Time.DeltaTime * scrollLerpSpeed);

            offset = Utils.Lerp(offset, scrollTarget, 20.0f * Time.DeltaTime);

            if (Utils.Distance(offset, scrollTarget) < 1f)
            {
                offset = scrollTarget;
            }
        }

        private void Render(SyntaxNode node)
        {
            if (node.ChildNodes().Any())
            {
                foreach (var c in node.ChildNodes())
                {
                    Render(c);
                }
            }
            else
            {
                var tokens = node.GetAnnotatedNodesAndTokens().Select(s => s.Span);
                var text = node.GetText();
                int c = 5;
            }
        }
    }
}