﻿using Nurose.Core;
using Somi.Core;
using System.IO;

namespace Somi.DefaultPlugins
{
    public class FileSavingService : IService
    {
        public SomiContext SomiContext { get; set; }

        public void Draw()
        {
            if(NuroseMain.Window.Input.IsKeyPressed(Key.S) && (NuroseMain.Window.Input.IsKeyHeld(Key.LeftControl) | NuroseMain.Window.Input.IsKeyHeld(Key.LeftControl)))
            {
                SaveCurrentSelectedFile();
            }
        }

        public void SaveCurrentSelectedFile()
        {
            ContextService contextService = SomiContext.Services.Get<ContextService>();
            var text = contextService.SourceText;
            File.WriteAllText(contextService.SelectedFilePath, text.ToString());
        }
    }
}
