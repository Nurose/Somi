﻿using Nurose.Core;
using Somi.Core;

namespace Somi.DefaultPlugins
{
    public class CustomRenderingService : IService
    {
        public SomiContext SomiContext { get; set; }
        public RenderQueue RenderQueue { get; private set; } = new();
        public ResourceManager ResourceManager { get; private set; } = new();


    }
}
