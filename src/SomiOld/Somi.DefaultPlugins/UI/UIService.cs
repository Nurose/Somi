﻿using Somi.Core;
using System.Collections.Generic;

namespace Somi.DefaultPlugins
{
    public class UIService : IService
    {
        public SomiContext SomiContext { get; set; }
        public List<UIElement> Elements { get; set; }
    }
}
