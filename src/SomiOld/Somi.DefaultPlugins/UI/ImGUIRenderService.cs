﻿using ImGuiNET;
using Nurose.Core;
using Nurose.ImGUI;
using Somi.Core;
using System;
using System.IO;
using System.Linq;

namespace Somi.DefaultPlugins
{
    public class ImGUIRenderService : IService
    {
        public SomiContext SomiContext { get; set; }
        private ImGuiContext ImGuiContext;
        private string searchInput = "";
        private bool IsOpen;
        private int currentItem = 0;
        public void Setup()
        {
            ImGuiContext = new();
            ImGuiContext.SetupFontTexture(SomiContext.GetService<CustomRenderingService>().ResourceManager, (int)NuroseMain.Monitor.ContentScale);
        }

        public void PreDraw()
        {
            InputState state = NuroseMain.Window.Input.State;


            if (NuroseMain.Window.Input.IsKeyPressed(Key.T) && NuroseMain.Window.Input.IsKeyHeld(Key.LeftControl) | NuroseMain.Window.Input.IsKeyHeld(Key.RightControl))
            {
                IsOpen = true;
            }

            if (NuroseMain.Window.Input.IsKeyPressed(Key.Enter))
            {
                IsOpen = false;

            }

            ImGuiContext.UpdateIO(NuroseMain.Window.Input, NuroseMain.Window.Size);

            if (ImGuiContext.WantCaptureMouse)
            {
                state.ButtonsHeld.Clear();
                state.ButtonsPressed.Clear();
                state.ButtonsReleased.Clear();
            }

            if (ImGuiContext.WantCaptureKeyboard)
            {
                state.CharInputs = "";
                state.KeysPressed.Clear();
                state.KeysReleased.Clear();
                state.KeysHeld.Clear();
            }


            ImGui.NewFrame();

            if (IsOpen)
            {
                var windowSize = new Vector2(800, 800);
                ImGui.SetNextWindowSize(new System.Numerics.Vector2(windowSize.X, windowSize.Y));
                ImGui.SetNextWindowPos(new System.Numerics.Vector2(NuroseMain.Window.Size.X / 2 - windowSize.X / 2, NuroseMain.Window.Size.Y / 2 - windowSize.Y / 2));
                ImGui.Begin("Search");
                ImGui.Text("Search:");
                ImGui.SameLine();
                ImGui.SetKeyboardFocusHere();

                ContextService contextService = SomiContext.GetService<ContextService>();
                string[] items;
                string[] paths;
                if (contextService.RoslynContextLoaded)
                {

                    var relevantDocs = contextService.RoslynContext.Workspace.CurrentSolution.Projects
                                        .SelectMany(p => p.Documents)
                                        .Where(p => p.Name.ToUpperInvariant().Contains(searchInput.ToUpperInvariant())).ToArray();

                    items = relevantDocs.Select(p => p.Name).ToArray();
                    paths = relevantDocs.Select(p => p.FilePath).ToArray();
                }
                else
                {
                    paths = Directory.GetFiles(Environment.CurrentDirectory, "*.cs", SearchOption.AllDirectories).ToArray();
                    items = paths.Select(p => Path.GetFileName(p)).ToArray();
                }



                if (ImGui.InputText("", ref searchInput, 10000) && items.Length > 0)
                {
                    contextService.SwapToDocument(paths[0]);
                }

                foreach (var item in items.Take(20))
                {
                    ImGui.Text(item);
                }
                //ImGui.ListBox("", ref currentItem, items, items.Length);
            }
        }

        public void Draw()
        {
            CustomRenderingService customRenderingService = SomiContext.GetService<CustomRenderingService>();
            ImGuiRenderTask imguiRenderTask = ImGuiContext.GetRenderTask(customRenderingService.ResourceManager, 1);
            customRenderingService.RenderQueue.StartGroup(0);
            customRenderingService.RenderQueue.Add(imguiRenderTask);
            customRenderingService.RenderQueue.EndGroup();
        }
    }
}
