﻿using System.Collections.Generic;

namespace Somi.DefaultPlugins
{
    public abstract class UIElement
    {
        public List<UIElement> Children;
        public abstract void Draw();
        public abstract void Update();
    }
}
