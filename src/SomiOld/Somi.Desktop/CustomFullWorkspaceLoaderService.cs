﻿using Microsoft.Build.Evaluation.Context;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using net.r_eg.MvsSln;
using Nurose.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Somi.Core
{

    struct NugetDllInfo
    {
        public string Name;
        public string Version;
    }

    public class CustomFullWorkspaceLoaderService : IWorkspaceLoaderService
    {
        public SomiContext SomiContext { get; set; }

        public AdhocWorkspace Load(string solutionFilePath)
        {
            MSBuildLocator.RegisterDefaults();
            var sln = new Sln(Path.GetFullPath(solutionFilePath), SlnItems.All).Result;
            AdhocWorkspace workspace = new();

            var projectInfos = new List<ProjectInfo>();
            foreach (var projectInfo in sln.Env.Projects)
            {
                if (projectInfos.Any(p => p.Name == projectInfo.ProjectName))
                    continue;

                var documents = new List<DocumentInfo>();
                var projectId = ProjectId.CreateFromSerialized(Guid.Parse(projectInfo.ProjectGuid));
                foreach (var codeFilePath in Directory.GetFiles(Path.GetDirectoryName(projectInfo.ProjectFullPath), "*.cs", SearchOption.AllDirectories))
                {
                    if (codeFilePath.Contains("\\bin\\") || codeFilePath.Contains("\\obj\\"))
                        continue;
                    var docInfo = DocumentInfo.Create(DocumentId.CreateNewId(projectId), Path.GetFileName(codeFilePath), filePath: codeFilePath, loader: new FileTextLoader(codeFilePath, Encoding.Default));
                    documents.Add(docInfo);
                }

                var projectRefs = sln.ProjectDependencies.Dependencies[projectInfo.ProjectGuid];
                var projectReferences = projectRefs.Select(r => new ProjectReference(ProjectId.CreateFromSerialized(Guid.Parse(r)))).ToArray();

                MetadataReference[] references = new MetadataReference[]
                {
                    MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
                    MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location)
                };
                var dllReferences = projectInfo.GetReferences().Select(p => p.meta.First().Value.evaluated).Select(a => MetadataReference.CreateFromFile(Path.Combine(projectInfo.ProjectPath, a))).ToList();

                var nugetReferences = projectInfo.GetPackageReferences().Select(p => new NugetDllInfo
                {
                    Name = p.evaluatedInclude,
                    Version = p.meta["Version"].evaluated
                }).ToList();


                foreach (var package in nugetReferences)
                {
                    ResolveReference(dllReferences, package);

                }
                projectInfos.Add(ProjectInfo.Create(projectId, VersionStamp.Default, projectInfo.ProjectName, "", LanguageNames.CSharp,
                    filePath: projectInfo.ProjectFullPath,
                    documents: documents,
                    projectReferences: projectReferences,
                    metadataReferences: references.Concat(dllReferences).ToArray()
                    ));
            }
            workspace.AddSolution(SolutionInfo.Create(SolutionId.CreateNewId(), VersionStamp.Default, filePath: solutionFilePath, projects: projectInfos));
            return workspace;
        }

        private void Mbw_WorkspaceFailed(object sender, WorkspaceDiagnosticEventArgs e)
        {
            throw new NotImplementedException();
        }

        private static void ResolveReference(List<PortableExecutableReference> dllReferences, NugetDllInfo package)
        {
            string userProfilePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string path = $"{userProfilePath}\\.nuget\\packages\\{package.Name.ToLowerInvariant()}\\{package.Version}\\lib";
            string genericDllpath = $"{path}\\{package.Name}.dll";
            if (File.Exists(genericDllpath))
            {
                dllReferences.Add(MetadataReference.CreateFromFile(path));
                return;
            }
            else
            {
                try
                {
                    dllReferences.Add(MetadataReference.CreateFromFile($"{path}\\netcore50\\{package.Name}.dll"));

                }
                catch (Exception e)
                {
                    Logger.LogWarning($"Could not load package {package.Name}");
                }
                return;
            }
        }
    }
}