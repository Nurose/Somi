﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Broslyn;
using Buildalyzer;
using Buildalyzer.Workspaces;
using Microsoft.Build.Construction;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis.Host.Mef;
using net.r_eg.MvsSln;
using Nurose.Core;
using Somi.Core;

namespace Somi.Desktop
{



    class Program
    {
        static void Main(string[] args)
        {
            NuroseMain.SetWindowFactory<Nurose.OpenTK.OpenTKWindowFactory>();
            NuroseMain.SetAudioFactory<NoAudioFactory>();

            var scale = NuroseMain.Monitor.ContentScale;
            NuroseMain.Create(new WindowConstructorArgs
            {
                Title = $"Somi",
                Size = new Vector2Int((int)(1280 * scale), (int)(720 * scale)),
                StartCentered = false,
                NumberOfAntiAliasSamples = 0,
                WindowBorder = WindowBorder.Hidden,
            });

            var entryLogic = NuroseMain.AddLogic<EntryLogic>();
            if (args.Length > 0)
            {
                // var host = MefHostServices.Create(MefHostServices.DefaultAssemblies);
                // AnalyzerManager manager = new AnalyzerManager(args[0]);
                //  Context.Workspace = manager.GetWorkspace();

                var ContextService = entryLogic.SomiContext.GetService<ContextService>();


                ContextService.SwapToDocument(Directory.GetFiles(Environment.CurrentDirectory, "*Program.cs", SearchOption.AllDirectories).First());


                entryLogic.SomiContext.GetService<BackgroundTasksService>().Enqeue(new System.Threading.Tasks.Task(() =>
                {
                    ContextService.LoadRoslynContext(new CustomFullWorkspaceLoaderService().Load(args[0]));
                    var workspace = ContextService.RoslynContext.Workspace;
                }));

            }
            else
            {
            }
            NuroseMain.Start();
        }
    }
}