﻿using System;
using Nurose.Console;
using Nurose.Core;
using Nurose.ECS; 
using Somi.Core;
using Somi.DefaultPlugins;
using System.Linq;
namespace Somi.Desktop
{


    class EntryLogic : IEntryLogic
    {
        public Input Input { get; set; }
        public IRenderTarget RenderTarget { get; set; }
        private Editor editor;
        private int i = 0;
        private WindowBorderRenderer windowBorder;
        private ConsoleHandler consoleHandler = new(null, Fonts.Default);
        public SomiContext SomiContext = new SomiContext();
        private ConsoleData consoleData = new()
        {
            LineHeight = 21,
        };

        public EntryLogic()
        {
            SomiContext.Services.SomiContext = SomiContext;
            SomiContext.Services.Add(new ContextService());
            SomiContext.Services.Add(new CustomRenderingService());
            SomiContext.Services.Add(new ImGUIRenderService());
            SomiContext.Services.Add(new FileSavingService());
            SomiContext.Services.Add(new BackgroundTasksService());
            SomiContext.Services.UpdateContent();
        }

        public void Start()
        {
            var scale = NuroseMain.Monitor.ContentScale;
            Input.AcceptRepeatedKeys = true;
            var lh = (int)(21 * scale);
            editor = new Editor(lh, SomiContext);
            windowBorder = new WindowBorderRenderer();
            NuroseMain.Window.Size = new Vector2Int((int)(1280 * scale), (int)(720 * scale));
            NuroseMain.Window.VSync = false;
            consoleHandler.RegisterWithLogger(consoleData);
            consoleData.LineHeight = (int)(14*scale);
            consoleData.Height = 500;
        }

        public void Update()
        {
        }
        
        public void Render()
        {

            Time.DeltaTime = Utils.MinMax(0, 1 / 30f, Time.DeltaTime);

            var renderQueue = SomiContext.GetService<CustomRenderingService>().RenderQueue;
            consoleHandler.UpdateInput(consoleData, Input);
            editor.Position = new Vector2Int(windowBorder.Width, windowBorder.TopBorderHeight);
            editor.Size = new Vector2Int(NuroseMain.Window.Size.X - windowBorder.Width * 2, NuroseMain.Window.Size.Y - windowBorder.Width - windowBorder.TopBorderHeight);

            if (Input.IsKeyPressed(Key.W))
            {
                i++;
            }

            foreach (var service in SomiContext.Services)
                service.PreDraw();

            renderQueue.StartGroup(0);
            renderQueue.StartPixelSpace();
            renderQueue.DrawRect(Vector2Int.Zero, NuroseMain.Window.Size, Color.FromHexString("add6ff26"));
            editor.Draw();
            windowBorder.Draw(renderQueue, SomiContext.GetService<ContextService>().SelectedFilePath);
            renderQueue.EndPixelSpace();
            renderQueue.EndGroup();


            foreach (var service in SomiContext.Services)
                service.Draw();

            RenderTarget.Render(renderQueue);
            consoleHandler.Draw(consoleData, renderQueue, new ResourceManager());
            RenderTarget.Render(renderQueue);
            Input.RefreshInput();
        }
    }
}