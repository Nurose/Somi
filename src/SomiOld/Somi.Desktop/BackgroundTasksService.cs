﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Somi.Core
{
    public class BackgroundTasksService : IService
    {
        public SomiContext SomiContext { get; set; }

        private ConcurrentQueue<Task> tasks = new();
        
        public void Enqeue(Task task)
        {
            tasks.Enqueue(task);
        }

        public void Setup()
        {
            var t = new Thread(() =>
             {
                 var q = tasks;

                 while (true)
                 {
                     if (q.Count > 0)
                     {
                         tasks.TryDequeue(out Task task);
                         task.RunSynchronously();
                     }
                     else
                     {
                         Thread.Sleep(50);
                     }
                 }
             });
            t.IsBackground = true;
            t.Start();
        }
    }
}