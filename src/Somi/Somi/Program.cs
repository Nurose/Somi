﻿using Nurose.Core;
using Nurose.OpenTK;

namespace Somi
{
    public class EntryLogic : IEntryLogic
    {
        public Input Input { get; set; }
        public IRenderTarget RenderTarget { get; set; }

        public void Render()
        {
        }

        public void Start()
        {
        }

        public void Update()
        {
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            NuroseMain.SetAudioFactory<NoAudioFactory>();
            NuroseMain.SetWindowFactory<OpenTKWindowFactory>();
            NuroseMain.Create(new WindowConstructorArgs
            {
                Title = "Nurose",
                Size = new Vector2Int(1920,1080),
                StartCentered = true,
                WindowBorder = WindowBorder.Fixed,
                NumberOfAntiAliasSamples = 8
            });
            NuroseMain.AddLogic<EntryLogic>();
            NuroseMain.Start();
        }
    }
}