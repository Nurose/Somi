using System.Runtime.CompilerServices;
using System.Collections.Concurrent;
using System;
using System.Collections.Generic;

// This is a sample project used to test Somi functionalities.

Datastruct datastruct = new Datastruct();
datastruct.A += 1;
datastruct.B = 3123 + (int)MathF.Sin(23);
datastruct.C = "Test string";

var refType = new ConcurrentDictionary<int, int>();
var valueType = new Datastruct();
var primitiveType = 55;

List<int> list = new List<int>();
var c = "test";


/// <summary>
/// Multiline 
/// documentations.
/// Test.
/// </summary>
public class ACoolClass
{
    public int InlinedProperty
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        get
        {
            return 45;
        }
    }
      
    public int GetMax<T>(T a, T b)
    {
       return 5; 
    }
}

